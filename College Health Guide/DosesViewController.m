//
//  SecondViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "DosesViewController.h"

@interface DosesViewController ()

@end

@implementation DosesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int screenWidth = self.view.bounds.size.width;
    int screenHeight = self.view.bounds.size.height;
    
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    UILabel* doses = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 80)];
    doses.text = @"Maximum Doses & Side Effects";
    doses.font = [UIFont systemFontOfSize:25];
    doses.textColor = [UIColor whiteColor];
    doses.textAlignment = NSTextAlignmentCenter;
    doses.backgroundColor = [UIColor clearColor];
    CAGradientLayer* topGradient = [CAGradientLayer layer];
    topGradient.frame = doses.frame;
    topGradient.colors = gradient.colors;
    [self.view.layer insertSublayer:topGradient above:NULL];
    [self.view addSubview:doses];
    [self.view bringSubviewToFront:doses];
    /*UILabel* bk = [[UILabel alloc] initWithFrame:doses.frame];
    bk.text = doses.text;
    bk.font = doses.font;
    bk.textColor = [UIColor whiteColor];
    bk.textAlignment = doses.textAlignment;
    bk.backgroundColor = doses.backgroundColor;
    bk.center = CGPointMake(bk.center.x-2, bk.center.y-1);
    [self.view addSubview:bk];*/
    
    //Create label for dose information
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, 0, screenWidth, 2000)];
    label.numberOfLines = 0;
    label.text = @"\n Ibuprofen (Motrin) 600 mg (3 tablets)\n Every 6 hours\n Can cause indigestion/stomach ache.\n\n\n Acetaminophen (Tylenol) 1000 mg (2 tablets)\n Every 6 hours\n\n\n Diphenhydramine (Benedryl) 25 mg\n Every 6 hours\n Causes sleepiness and fatigue; constipation.\n\n\n Decongestant (Phenylephrine): 10 mg\n Every 4 hours\n Heart racing or jittery; will keep you awake.\n";
    label.textColor = [UIColor blackColor];
    label.layer.cornerRadius = 5;
    label.layer.masksToBounds = YES;
    label.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
    [label sizeToFit];
    label.frame = CGRectMake(screenWidth*0.02, 0, screenWidth*0.96, label.frame.size.height);
    //Create scroll view to show all of label if needed
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 90, screenWidth, screenHeight-129)];
    [scrollView addSubview:label];
    scrollView.contentSize = label.frame.size;
    [self.view addSubview:scrollView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
