//
//  FirstLaunchScreen.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/1/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "FirstLaunchScreen.h"

@implementation FirstLaunchScreen

NSString* username;
NSString* email;
NSDate* dob;
UILabel* nameLabel;
UITextField* nameField;
UILabel* emailLabel;
UITextField* emailField;
UILabel* dobLabel;
UIDatePicker* dobField;


-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.layer insertSublayer:gradient atIndex:0];
    [self getUserInfo];
    return self;
}


//Create text labels and fields for name, email, and DOB plus a submit button
- (void)getUserInfo {
    int top = 30;
    int screenWidth = self.bounds.size.width;
    
    nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, top, screenWidth*0.8, 30)];
    nameLabel.text = @"Name:";
    [self addSubview:nameLabel];
    
    nameField = [[UITextField alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+30, screenWidth*0.8, 40)];
    nameField.borderStyle = UITextBorderStyleRoundedRect;
    nameField.placeholder = @"Enter your name";
    [self addSubview:nameField];
    
    emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+90, screenWidth*0.8, 30)];
    emailLabel.text = @"Email:";
    [self addSubview:emailLabel];
    
    emailField = [[UITextField alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+120, screenWidth*0.8, 40)];
    emailField.borderStyle = UITextBorderStyleRoundedRect;
    emailField.placeholder = @"name@example.com";
    [self addSubview:emailField];
    
    dobLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+180, screenWidth*0.8, 30)];
    dobLabel.text = @"Date of Birth:";
    [self addSubview:dobLabel];
    
    dobField = [[UIDatePicker alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+210, screenWidth*0.8, 100)];
    dobField.layer.cornerRadius = 5;
    dobField.layer.masksToBounds = YES;
    dobField.backgroundColor = [UIColor whiteColor];
    dobField.datePickerMode = UIDatePickerModeDate;
    [self addSubview:dobField];
    
    _submitButton = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.1, top+330, screenWidth*0.8, 40)];
    [_submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    _submitButton.backgroundColor = [UIColor whiteColor];
    [_submitButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    _submitButton.layer.cornerRadius = 5;
    _submitButton.layer.masksToBounds = YES;
    [self addSubview:_submitButton];
    [_submitButton addTarget:self action:@selector(submitTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    [_submitButton addTarget:self action:@selector(submitPressDown:) forControlEvents:UIControlEventTouchDown];
}


//Change submit button color when pressed down
-(IBAction)submitPressDown:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:227/255.0 green:231/255.0 blue:232/255.0 alpha:1];
}

//Save fields when submit button is clicked
-(IBAction)submitTouchUp:(UIButton *)sender {
    username = nameField.text;
    email = emailField.text;
    dob = dobField.date;
    sender.backgroundColor = [UIColor whiteColor];
    if(username.length < 2) nameLabel.textColor = [UIColor redColor];
    else nameLabel.textColor = [UIColor blackColor];
    if(email.length < 7 || ![email containsString:@"@"] || ![email containsString:@"."]) emailLabel.textColor = [UIColor redColor];
    else emailLabel.textColor = [UIColor blackColor];
    if(username.length > 1 && email.length > 6 && [email containsString:@"@"] && [email containsString:@"."]) [self removeFromSuperview];
}

@end
