//
//  TipsViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "TipsViewController.h"

@interface TipsViewController ()

@end

@implementation TipsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int screenWidth = self.view.bounds.size.width;
    int screenHeight = self.view.bounds.size.height;
    
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    UILabel* tips = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 80)];
    tips.text = @"Hints on Being Smart";
    tips.font = [UIFont systemFontOfSize:30];
    tips.textColor = [UIColor whiteColor];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.backgroundColor = [UIColor clearColor];
    CAGradientLayer* topGradient = [CAGradientLayer layer];
    topGradient.frame = tips.frame;
    topGradient.colors = gradient.colors;
    [self.view.layer insertSublayer:topGradient above:NULL];
    [self.view addSubview:tips];
    [self.view bringSubviewToFront:tips];
    
    //Create label for dose information
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, 0, screenWidth, 20000)];
    label.numberOfLines = 0;
    label.text = @"\n • Brown Bottle of beer. This is the best as you can pick one up at a party (or bring your own), go to the bathroom, empty it out, and drink water and no one knows.\n\n\n • NEVER let anyone “hold” your drink for you, even if it is water. It is too easy to add something to it.\n\n\n • Always go to a party with someone you trust. Never leave (and NEVER walk home) alone. Work this out BEFORE you go with your friend, because people do stupid things once they have one drink.\n\n\n • Alcohol “dis-inhibits” the brain. It allows you to say “OK, sure” when the non drinking brain would always say NO. Make NO decision after you have had a drink except what you want on your pizza.\n\n\n • If your friend has had too much to drink, DO NOT LEAVE THEM ALONE. If they are breathing less than 10 breaths per minute, wake them up. If they are not breathing enough or are vomiting but not waking up, call 911!\n\n\n • If you have had more than 2 drinks in a night, take 2 Acetaminophen (Tylenol) 1000 mg (2 tablets) and a LARGE drink of water before you go to bed.\n\n\n • NEVER have more than 4 drinks in 24 hours. Your body cannot handle it. Doctors ask people “Have you ever had 5 or more drinks in 24 hours” to screen for alcoholism.\n\n\n • Never combine alcohol with any other drug.\n\n\n • 1 in 10 Condoms Break. Herpes and AIDS cannot be cured. You die with them. No unprotected sex.\n\n\n • NEVER have sex unless you feel ready to be a parent. All forms of birth control fail. So, unless you feel ready to change your life to have a baby, don’t.\n\n\n • Never, ever drive after having more than 2 drinks or after smoking marijuana. This DOUBLES the rate of death from car accidents.\n\n";
    label.textColor = [UIColor blackColor];
    label.layer.cornerRadius = 5;
    label.layer.masksToBounds = YES;
    label.backgroundColor = [UIColor colorWithWhite:1 alpha:0.6];
    [label sizeToFit];
    label.frame = CGRectMake(screenWidth*0.02, 0, screenWidth*0.96, label.frame.size.height+40);
    //Create scroll view to show all of label if needed
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 90, screenWidth, screenHeight-129)];
    [scrollView addSubview:label];
    scrollView.contentSize = label.frame.size;
    [self.view addSubview:scrollView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
