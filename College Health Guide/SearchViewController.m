//
//  FirstViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

FirstLaunchScreen* firstLS;

- (void)viewDidLoad {
    [super viewDidLoad];
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    UILabel* search = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 80)];
    search.text = @"Search Symptoms";
    search.font = [UIFont systemFontOfSize:30];
    search.textColor = [UIColor whiteColor];
    search.textAlignment = NSTextAlignmentCenter;
    search.backgroundColor = [UIColor clearColor];
    CAGradientLayer* topGradient = [CAGradientLayer layer];
    topGradient.frame = search.frame;
    topGradient.colors = gradient.colors;
    [self.view.layer insertSublayer:topGradient above:NULL];
    [self.view addSubview:search];
    [self.view bringSubviewToFront:search];
    _table.frame = CGRectMake(0, 80, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-129);
    _symptoms = [[NSArray alloc] initWithObjects:@"Sore Throat", @"Cold/Runny Nose/Sinus Pressure", @"Cough", @"Allergic Reaction (bee sting, poison ivy, etc.)", @"Seasonal Allergies", @"Ear Ache", @"Goopy and Crusty (Pink) Eye", @"Nausea and Vomiting", @"Diarrhea", @"Black Poop", @"Stomach Ache", @"Constipation", @"Weight Gain", @"Indigestion/Heart Burn", @"Cold/Shivering", @"Headache", @"Hangover", @"Sprained Ankle", @"Joint Pain", @"Neck Pain", @"Soreness After Workout", @"Urination Pain/Going Frequently", @"Vaginal Discharge", @"Hemorrhoids", @"Burn", @"Scrapes/Cuts/Rug Burn", @"Itchy Rash",  nil];
    
    _remedies = [[NSArray alloc] initWithObjects:@"Most times, just a cold, not Strep throat (which mostly happens to kids < 5 years old); sinus infection, allergy, cough induced. Check your temperature:\n-- If above 100.4, go to infirmary.\n-- If no fever, do you have:\n-- Use a Zinc Lozenge every 3-4 hours till better\n-- Post Nasal Drip: Likely a cold. Take 1 Decongestant (phenylephrine 10 mg) every 4 hours & Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours AS NEEDED. If it goes on for more than a week, could be sinus infection; go to infirmary.\n-- No post nasal drip. Could be an allergy or cold (see below).\n-- Allergy symptoms (sore throat, itchy eyes, runny nose), try Cetirizine (Zyrtec) 10 mg", @"Likely a virus. Take Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours or Acetaminophen (Tylenol) 1000 mg (2 pills) every 6 hours; and if runny nose, take 1 Decongestant every 4 hours.\n-- If spring or fall, likely allergy, see \"Seasonal Allergies\"\n-- If symptoms go on for > 5 days, go to infirmary.", @"Honey 2 Tbls every 4 hours. At night, try Diphenhydramine (Benedryl) 25 mg.", @"Take Diphenhydramine (Benedryl) 25 mg every as soon as you can, then every 6 hours.", @"Take Cetirizine (Zyrtec) 10 mg once a day.", @"(Rarely needs antibiotics)\n-- Both sides: likely allergies; see \"Seasonal Allergies\"\n-- One side: could be Eustachian tube dysfunction; Take 1 Decongestant (phenylephrine 10 mg) every 4 hours & Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours every 6 hours AS NEEDED. May need a nose spray; if goes on for more than a week without improvement, go to infirmary", @"Virus; use saline drops and wash hands all the time or you will give it to the other eye or anyone you touch. Sometimes antibiotic ointment or drops are used to help the secondary infection but are rarely needed.", @"Normally caused by a virus, or drinking too much alcohol. Drink room temperature water or sports drink in sips. If severe, go to infirmary or try diphenhydramine (Benedryl) 12.5 mg every 6 hours. If you feel hungry, only dry bread or toast with jelly (no butter or peanut butter) or slowly munch on pretzels.\n-- If it is associated with stomach pain that was initially all over and now the pain is mostly in the right lower quadrant of your stomach, go to the emergency room immediately.", @"Normally caused by a virus; drink non carbonated liquids like Sports Drink or decaf/herbal tea with sugar; only eat things without fat. Pretzels, dry white rice, bananas, dry toast with jelly. If bad, take diphenhydramine 12.5 every 6 hours. NO: Milk, spicy or fatty foods, etc. for at least 24 hours", @"Go to infirmary. Maybe gastroenteritis. NO Ibuprofen (Motrin)", @"Normally due to something you ate. Take 1-2 Tums every 4 hours as needed.", @"Eat more fruits/veggies, stool softener, drink lots more water", @"Drink 16 oz of water before each meal; eat lots of fruits/veggies, don’t skip meals and never let yourself get “starving.” If you do feel really hungry, drink 16 oz. water, and eat a piece of fruit.", @"Take 1-2 Tums every 4 hours as needed; if than a few days, go to infirmary.", @"Also very tired when no one else is cold. You probably have a fever. Check your temperature. If no cold symptoms, go to infirmary. If you have a cold symptoms, see above", @"-- Both sides, like a band around head, is Muscle Contraction (tension) headache. Take Ibuprofen (Motrin) 600 (3 pills) every 6 hours AND Acetaminophen (Tylenol) 1000 mg (2 pills) every 6 hours; try to take as soon as you feel the headache starting coming on. If happening most days of the week, go to the infirmary.\n-- One side, associated with eye pain (esp. to bright light) and possibly nausea, could be a Migraine. Take Migraine Relief (remember, this has caffeine) as soon as you feel it coming on; if your stomach is upset, take it with 12.5 of Diphenhydramine (Benedryl), and go to sleep.", @"Take 2 Acetaminophen (Tylenol) 1000 mg (2 tablets) every 6 hours, drink lots of water or sports drink, and be patient.", @"Leave your shoe ON until you can put it up in the air with an ice pack. Then, do not walk on it unless you wrap it in the Ace Bandage or Air Cast. If you cannot walk on it without terrible pain, then you may need an x-ray. Take Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours. Most sprains take 6 weeks to heal. Often, you need physical therapy to get it to fully heal, so if still hurting after 4 weeks, go to infirmary.", @"(Knee, Shoulder) from overuse: RICE: R: REST it. For a few weeks. Seriously. I: Ice for first 24 hours, then ice or heat thereafter, which ever feels better; C: Compression; for ankle and knee, wrap in ACE bandage; E: Elevate. Keep a knee or ankle up. Ibuprofen 600 mg (4 tablets) every 6 hours; get checked out in the infirmary if not improving.", @"Often related to Tension headaches. Take Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours, GENTLE stretching toward the side that hurts (not away, as most people think) and hold it in the position for 2 minutes (10-20 deep breaths).", @"Drink lots of water, Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours or Acetaminophen (Tylenol) 1000 mg (2 pills) every 6 hours", @"Typically a urinary tract infection. May see blood, often you feel the urge to go often but only go a little. They do NOT get better on their own. You must go to the infirmary and be treated with an antibiotic. You can take Azo 2 tablets every 4 hours to relieve symptoms (this may turn your urine orange; that is OK), but you still must go to the infirmary.", @"Lots of things can cause this and it does not go away on its own. Go to the Infirmary.", @"Water, stool softener, eat lots more vegetables (green, limit rice and potatoes), wipe gently, NO scratching, wipe on with toilet paper hydrocortisone cream 1% for a 2-3 days; if not better, go to infirmary.", @"Put Ice on it immediately or, if you are close to your room, first 1% hydrocortisone cream, then Ice and Ibuprofen (Motrin) 600 mg (3 pills) every 6 hours.", @"Wash with warm water and soap. Gently dab with Hydrogen Peroxide, then cover with bacitracin Ointment and bandage twice a day for 3 days. Any redness getting bigger in size around cut, or fever, go to Infirmary or Emergency room. Don’t wait.", @"Like a contact dermatitis. Use Hydrocortisone Cream 1% every 4 hours; if not better in a few days, or every associated with a fever, go to infirmary.", nil];
}

//Call disclaimerPopUp and getUserInfo only for first launch
- (void)viewDidAppear:(BOOL)animated {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FirtLaunch"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirtLaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self disclaimerPopUp];
        firstLS = [[FirstLaunchScreen alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:firstLS];
        [self.view bringSubviewToFront:firstLS];
    }
}

//Makes window pop up with a disclaimer that the user must agree to
- (void)disclaimerPopUp {
    UIAlertController* disclaimer = [UIAlertController alertControllerWithTitle:@"Disclaimer"
                                                        message:@"Do you agree to these conditions..."
                                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* agree= [UIAlertAction actionWithTitle:@"Agree"
                                        style:UIAlertActionStyleDefault
                                        handler:nil];
    [disclaimer addAction:agree];
    [self presentViewController:disclaimer animated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _symptoms.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    cell.textLabel.text = [_symptoms objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:_symptoms[indexPath.row] message:_remedies[indexPath.row] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [messageAlert show];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
