//
//  FirstViewController.h
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstLaunchScreen.h"

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray* symptoms;

@property (nonatomic, strong) NSArray* remedies;

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

