//
//  FirstLaunchScreen.h
//  College Health Guide
//
//  Created by Steven Novitske on 4/1/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstLaunchScreen : UIView

@property (nonatomic, strong)UIButton* submitButton;

-(id)initWithFrame:(CGRect)frame;

@end
