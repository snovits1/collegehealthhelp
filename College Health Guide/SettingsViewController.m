//
//  SettingsViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int screenWidth = self.view.bounds.size.width;
    int screenHeight = self.view.bounds.size.height;
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    UILabel* settings = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 80)];
    settings.text = @"Settings";
    settings.font = [UIFont systemFontOfSize:30];
    settings.textColor = [UIColor whiteColor];
    settings.textAlignment = NSTextAlignmentCenter;
    settings.backgroundColor = [UIColor clearColor];
    CAGradientLayer* topGradient = [CAGradientLayer layer];
    topGradient.frame = settings.frame;
    topGradient.colors = gradient.colors;
    [self.view.layer insertSublayer:topGradient above:NULL];
    [self.view addSubview:settings];
    [self.view bringSubviewToFront:settings];
    
    UILabel* whatever = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, 90, screenWidth*0.8, 40)];
    whatever.text = @"Put settings info here";
    [self.view addSubview:whatever];
    
    UILabel* copyright = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.1, screenHeight-80, screenWidth*0.8, 20)];
    copyright.text = @"Copyright © 2017 Steven Novitske. All rights reserved.";
    copyright.font = [UIFont systemFontOfSize:10];
    [copyright setTextColor:[UIColor blackColor]];
    copyright.textAlignment = NSTextAlignmentCenter;
    copyright.layer.cornerRadius = 5;
    copyright.layer.masksToBounds = YES;
    copyright.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
    [self.view addSubview:copyright];
                                                                   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
