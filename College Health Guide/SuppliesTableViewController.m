//
//  SuppliesTableViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/4/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "SuppliesTableViewController.h"

@interface SuppliesTableViewController ()

@end

@implementation SuppliesTableViewController

-(void)viewDidLoad {
    _data = [NSArray arrayWithObjects:@"First Aid Kit", @"Bandaids", @"Bacitracin (NOT Neosporin) ointment", nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier =@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text=[_data objectAtIndex:indexPath.row];
    
    return cell;
}

@end
