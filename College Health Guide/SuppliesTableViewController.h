//
//  SuppliesTableViewController.h
//  College Health Guide
//
//  Created by Steven Novitske on 4/4/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface SuppliesTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, strong) NSArray* data;

@end
