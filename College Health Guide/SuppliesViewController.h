//
//  SuppliesViewController.h
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuppliesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray* supplies;

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

