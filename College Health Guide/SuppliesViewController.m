//
//  SuppliesViewController.m
//  College Health Guide
//
//  Created by Steven Novitske on 4/3/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "SuppliesViewController.h"
#import "SuppliesTableViewController.h"

@interface SuppliesViewController ()

@end

@implementation SuppliesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:54/255.0 green:206/255.0 blue:227/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithRed:199/255.0 green:245/255.0 blue:245/255.0 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 80)];
    label.text = @"Medical Supplies";
    label.font = [UIFont systemFontOfSize:30];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    CAGradientLayer* topGradient = [CAGradientLayer layer];
    topGradient.frame = label.frame;
    topGradient.colors = gradient.colors;
    [self.view.layer insertSublayer:topGradient above:NULL];
    [self.view addSubview:label];
    [self.view bringSubviewToFront:label];
    _table.frame = CGRectMake(0, 80, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-129);
    _supplies = [[NSArray alloc] initWithObjects:@"First Aid Kit", @"Bandaids", @"Bacitracin (NOT Neosporin) ointment", @"Ibuprofen (Motrin, Advil) 200 mg tablets", @"Acetaminophen (Tylenol) 500 mg tablets", @"Migraine relief medicine (like Excedrine)", @"Cetirizine (Zyrtec) 10 mg", @"Diphenhydramine (Benedryl) 12.5 mg", @"Tums", @"Azo Urinary Pain Relief", @"Hydrocortisone Cream 1%", @"Thermometer", @"Cough Drops (menthol helps for congestion)", @"ACE bandage", @"Packets of dry sports drink mix", @"Zinc Lozenges (like Coldeeze)", nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _supplies.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
    cell.textLabel.text = [_supplies objectAtIndex:indexPath.row];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
